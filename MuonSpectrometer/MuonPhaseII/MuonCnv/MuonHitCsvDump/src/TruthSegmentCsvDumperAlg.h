#ifndef MUONCSVDUMP_TruthSegmentCsvDumperAlg_H
#define MUONCSVDUMP_TruthSegmentCsvDumperAlg_H
/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include <AthenaBaseComps/AthAlgorithm.h>
#include <MuonIdHelpers/IMuonIdHelperSvc.h>
#include <StoreGate/ReadHandleKeyArray.h>

#include <xAODMuon/MuonSegmentContainer.h>
#include <MuonReadoutGeometryR4/MuonDetectorManager.h>


namespace MuonR4{
class TruthSegmentCsvDumperAlg: public AthAlgorithm {

  public:
  TruthSegmentCsvDumperAlg(const std::string& name, ISvcLocator* pSvcLocator);
  ~TruthSegmentCsvDumperAlg() = default;
  StatusCode initialize() override;
  StatusCode execute() override;
  
  private:
  SG::ReadHandleKey<xAOD::MuonSegmentContainer> m_inSegmentKey{
    this, "MuonTruthSegmentsKey", "TruthSegmentsR4"};
  ServiceHandle<Muon::IMuonIdHelperSvc> m_idHelperSvc{
    this, "MuonIdHelperSvc", "Muon::MuonIdHelperSvc/MuonIdHelperSvc"};
  const MuonGMR4::MuonDetectorManager* m_r4DetMgr{nullptr};
  size_t m_event{0};

};
}
#endif
