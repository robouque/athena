# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

AntiKt4TruthDressedWZJetsCPContent = [
"AntiKt4TruthDressedWZJets",
"AntiKt4TruthDressedWZJetsAux.pt.eta.phi.m.PartonTruthLabelID.HadronConeExclExtendedTruthLabelID.HadronConeExclTruthLabelID.TrueFlavor.GhostBHadronsFinalCount.GhostBHadronsFinal"
]

